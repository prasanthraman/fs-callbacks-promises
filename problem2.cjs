let fs = require("fs")
let path = require("path")

function problem2() {

    let readLipsum = new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, `lipsum`, `lipsum.txt`), { encoding: 'utf8' }, (err, data) => {
            if (err) {
                console.log("Can't read lipsum text file")
                reject(err)
            } else {
                console.log("Read lipsum text file!")
                data = (data.toUpperCase())
                resolve(data)
            }
        })
    })

    let writeUpperLipsum = (data) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, `lipsum`, `upper-lipsum.txt`), data, (err) => {
                if (err) {
                    console.log("Can't write upper-lipsum text file")
                    reject(err)
                } else {
                    resolve("Write Success for upper-lipsum")
                }
            })
        })
    }

    let appendUpperLipsum = () => {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, `lipsum`, `filenames.txt`), 'upper-lipsum.txt', (err) => {
                if (err) {
                    console.log("Can't append upper-lipsum text file")
                    reject(err)
                } else {
                    resolve("Appended upper-lipsum.txt to filenames")
                }
            })
        })
    }

    let readUpperLipsum = () => {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, `lipsum`, `upper-lipsum.txt`), { encoding: 'utf8' }, (err, data) => {
                if (err) {
                    console.log("Can't read upper lipsum text file")
                    reject(err)
                } else {
                    console.log("Read Upper-lipsum text file!")
                    data = data.toLowerCase()
                        .split(/(?<=[.!?])/)

                    data = data.reduce((acc, value) => {
                        if (value == " ") {
                            return acc
                        }
                        acc += `${value.trim()}\n`
                        return acc
                    }, "")
                    resolve(data)
                }
            })

        })

    }

    let writeLowerLipsum = (data) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, `lipsum`, `lower-lipsum.txt`), data, (err) => {
                if (err) {
                    console.log("Can't write lower-lipsum")
                    reject(err)
                } else {
                    resolve("Write success for lower-lipsum")
                }
            })

        })
    }

    let appendLowerLipsum = () => {
        return new Promise((resolve, reject) => {
            fs.appendFile(path.join(__dirname, `lipsum`, `filenames.txt`), `\nlower-lipsum.txt`, { encoding: 'utf8' }, (err) => {
                if (err) {
                    console.log("Can't append lower-lipsum to filenames.txt")
                    reject(err)
                } else {
                    resolve("Appended lower-lipsum to filenames.txt")
                }
            })

        })
    }

    let readLowerLipsum = () => {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, `lipsum`, `lower-lipsum.txt`), { encoding: 'utf-8' }, (err, data) => {
                if (err) {
                    console.log("Can't read lower-lipsum text file")
                    reject(err)
                }
                else {
                    console.log("Read lower-lipsum text file")
                    data = data.split('\n')
                        .sort()
                        .join('\n')
                    resolve(data)
                }
            })
        })

    }


    let writeSortedLipsum = (data) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(path.join(__dirname, `lipsum`, `sorted-lipsum.txt`), data, (err) => {
                if (err) {
                    console.log("Can't write sorted-lipsum")
                    reject(err)
                } else {
                    resolve("Write Success for sorted-lipsum")

                }
            })

        })
    }

    let appendSortedLipsum = () => {
        return new Promise((resolve, reject) => {
            fs.appendFile(path.join(__dirname, `lipsum`, `filenames.txt`), `\nsorted-lipsum.txt`, (err) => {
                if (err) {
                    console.log("Can't append sorted-lipsum to filenames.txt")
                    reject(err)
                } else {
                    resolve("Appended sorted-lipsum to filenames.txt")
                }
            })

        })
    }

    let readFileNames = () => {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(__dirname, `lipsum`, `filenames.txt`), { encoding: 'utf-8' }, (err, data) => {
                if (err) {
                    console.log("Can't read filenames.txt")
                    reject(err)
                } else {
                    console.log("Successfully read filenames.txt")
                    let fileNames = data.split(`\n`)
                    resolve(fileNames)

                }
            })

        })
    }


    readLipsum
        .then((data) => {
            //console.log(data)
            return writeUpperLipsum(data)
        })
        .then((status) => {
            console.log(status)
            return appendUpperLipsum()
        })
        .then((status) => {
            console.log(status)
            return readUpperLipsum()
        })
        .then((data) => {
            //console.log(data)
            return writeLowerLipsum(data)
        })
        .then((status) => {
            console.log(status)
            return appendLowerLipsum()
        })
        .then((status) => {
            console.log(status)
            return readLowerLipsum()
        })
        .then((data) => {
            return writeSortedLipsum(data)
        })
        .then((status) => {
            console.log(status)
            return appendSortedLipsum()
        })
        .then((status) => {
            console.log(status)
            return readFileNames()
        })
        .then((fileNames) => {
            console.log(fileNames)
            fileNames=fileNames.map((fileName) => {
                console.log("Started Deleting :", fileName)
                return deletefile(fileName)
            })
           return fileNames
        })
        .then((fileNames)=>{
            return Promise.all(fileNames)
        })
        .then(()=>{
            console.log("Deleted All files on filenames!")
        })

        .catch(err => console.log(err))



    function deletefile(fileName) {
        return new Promise((resolve,reject)=>{
            fs.unlink(path.join(__dirname, 'lipsum', fileName), (err) => {
                if (err) {
                    reject("Can't delete", fileName)
                } else {
                    resolve(`Deleted file : ${fileName}`)
                }
            })

        })

    }
}
module.exports = problem2