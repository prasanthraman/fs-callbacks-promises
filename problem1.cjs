let fs = require("fs")
let path = require("path")

function problem1(numberOfFiles) {

    let randomObj = { name: "randomJson" }
    let createPromises = Array(numberOfFiles)
        .fill()
    let deletePromises = Array(numberOfFiles)
        .fill()

    let createDirectory = new Promise((resolve, reject) => {

        fs.mkdir(path.join(__dirname, `json-directory`), (err) => {
            if (err) {
                console.log("Can't create directory!")
                reject(err)
            } else {
                resolve("Created a Directory!")
            }
        })

    })

    createDirectory
        .then((status) => {
            console.log('CreateDirectoryPromise:', status)
            createPromises = createPromises.map((_, index) => {
                return createFiles(`randomFile${index}.json`)
            })
        })
        .then(() => {
            return Promise.all(createPromises)
        })
        .then(() => {
            console.log("Created All files!")
        })
        .then(()=>{
            deletePromises=deletePromises.map((_,index)=>{
                return deleteFiles(`randomFile${index}.json`)
            })
        })
        .then(()=>{
            return Promise.all(deletePromises)
        })
        .then(()=>{
            console.log("Deleted All files!")
        })
        .catch((err) => {
            console.log(err)
        })



    function createFiles(fileName) {
        return new Promise((resolve, reject) => {
            fs.createWriteStream(path.join(__dirname, `json-directory`, fileName))
                .write(JSON.stringify(randomObj), (err) => {
                    if (err) {
                        console.log("Can't write", fileName)
                        reject(err)
                    } else {
                        console.log(`Write successfull for: ${fileName}`)
                        resolve(`Write successfull for: ${fileName}`)
                    }
                })
            console.log("Started creating file:", fileName)
        })
    }

    function deleteFiles(fileName) {
        return new Promise((resolve, reject) => {
            fs.unlink(path.join(__dirname, `json-directory`, fileName), (err) => {
                if (err) {
                    console.log("Can't delete", fileName)
                    reject(err)
                } else {

                    resolve(`Deleted file: ${fileName}`)
                }
            })
            console.log("Started Deleting file:", fileName)
        })
    }

}

module.exports = problem1